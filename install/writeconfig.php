<h2>Installing Clinical Trials Viewer</h2>
<?php

// delete config.php

if ( file_exists ( $_POST['abs_path'] . "config.php" ) ) {

    unlink ( $_POST['abs_path'] . "config.php" );

}

// write new config.php

$configphp = array ();

array_push ( $configphp, "<?php\n" );
array_push ( $configphp, "\n" );
array_push ( $configphp, "define('DB_USER', '" . $_POST['dbusername'] . "');\n" );
array_push ( $configphp, "define('DB_PASS', '" . $_POST['dbpassword'] . "');\n" );
array_push ( $configphp, "define('DB_NAME', '" . $_POST['dbname'] . "');\n" );
array_push ( $configphp, "define('DB_HOST', '" . $_POST['dbhost'] . "');\n" );
array_push ( $configphp, "define('ABS_PATH', '" . $_POST['abs_path'] . "');\n" );
array_push ( $configphp, "define('SITE_URL', '" . $_POST['site_url'] . "');\n" );
array_push ( $configphp, "\n" );
array_push ( $configphp, "include_once (ABS_PATH . \"functions.php\");\n" );
array_push ( $configphp, "\n" );
array_push ( $configphp, "?>" );


if ( file_put_contents ( $_POST['abs_path'] . "config.php", $configphp ) ) {

    echo "<p>Configuration file written &#x2713;</p>";

} else {

    echo "<p>Error writing configuration file</p>";
    
}

// delete old .htaccess

if ( file_exists ( $_POST['abs_path'] . ".htaccess" ) ) {

    unlink ( $_POST['abs_path'] . ".htaccess");
}

// write new .htaccess

$htaccess = array ();

array_push ( $htaccess, "# BEGIN Rewrite\n" );
array_push ( $htaccess, "\n" );
array_push ( $htaccess, "<IfModule mod_rewrite.c>\n" );
array_push ( $htaccess, "\n" );
array_push ( $htaccess, "RewriteEngine On\n" );
array_push ( $htaccess, "RewriteCond %{SERVER_PORT} 80\n" );
array_push ( $htaccess, "RewriteRule ^(.*)$ " . $_POST['site_url'] . "$1 [R,L]\n" );
array_push ( $htaccess, "\n" );
array_push ( $htaccess, "RewriteEngine On\n" );

if ( substr_count ( $_POST['site_url'], "/" ) > 3 ) { // if this is being installed in a subdirectory
    $subdir = substr ( $_POST['site_url'], strrpos ( $_POST['site_url'], "/", -2 ));
    array_push ( $htaccess, "RewriteBase " . $subdir . "\n" );
} else {
    array_push ( $htaccess, "RewriteBase /\n" );
}

array_push ( $htaccess, "RewriteCond %{REQUEST_FILENAME} !-f\n" );
array_push ( $htaccess, "RewriteCond %{REQUEST_FILENAME} !-d\n" );

if ( substr_count ( $_POST['site_url'], "/" ) > 3 ) { // if this is being installed in a subdirectory
    $subdir = substr ( $_POST['site_url'], strrpos ( $_POST['site_url'], "/", -2 ));
    array_push ( $htaccess, "RewriteRule ([0-9A-Za-z]+) " . $subdir . "index.php?address=$1 [L]\n" );
} else {
    array_push ( $htaccess, "RewriteRule ([0-9A-Za-z]+) /index.php?address=$1 [L]\n" );
}

array_push ( $htaccess, "\n" );
array_push ( $htaccess, "</IfModule>" );

if ( file_put_contents ( $_POST['abs_path'] . ".htaccess", $htaccess ) ) {
    echo "<p>Server access file written &#x2713;</p>";
} else {
    echo "<p>Error writing server access file</p>";    
}

// add new blank database schema

try { // change character encoding
    $dbh = new PDO('mysql:dbname=' . $_POST['dbname'] . ';host=' . $_POST['dbhost'], $_POST['dbusername'], $_POST['dbpassword'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $stmt = $dbh->prepare("ALTER SCHEMA database DEFAULT CHARACTER SET utf8 DEFAULT COLLATE latin1_general_ci;");

    if ( $stmt-> execute() ) {
	echo "<p>Character encoding set &#x2713;</p>";
    } else {
	echo "<p>Error setting character encoding</p>";
    }
    
    $dbh = null;
    
}
catch (PDOException $e) {
    
    echo $e->getMessage();
    
}

// start with the tables

try { // add ctg_trials table  
    $dbh = new PDO('mysql:dbname=' . $_POST['dbname'] . ';host=' . $_POST['dbhost'], $_POST['dbusername'], $_POST['dbpassword'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $stmt = $dbh->prepare("DROP TABLE IF EXISTS `ctg_trials`; CREATE TABLE `ctg_trials` ( `id` int(11) NOT NULL, `address` varchar(64) COLLATE utf8_bin NOT NULL, `NCT_Number` varchar(11) COLLATE utf8_bin NOT NULL, `Study_Title` varchar(500) COLLATE utf8_bin NOT NULL, `Study_URL` varchar(44) COLLATE utf8_bin NOT NULL, `Acronym` varchar(50) COLLATE utf8_bin NOT NULL, `Study_Status` varchar(25) COLLATE utf8_bin NOT NULL, `Brief_Summary` longtext COLLATE utf8_bin NOT NULL, `Study_Results` varchar(20) COLLATE utf8_bin NOT NULL, `Conditions` longtext COLLATE utf8_bin NOT NULL, `Interventions` longtext COLLATE utf8_bin NOT NULL, `Primary_Outcome_Measures` longtext COLLATE utf8_bin NOT NULL, `Secondary_Outcome_Measures` longtext COLLATE utf8_bin NOT NULL, `Other_Outcome_Measures` longtext COLLATE utf8_bin NOT NULL, `Sponsor` longtext COLLATE utf8_bin NOT NULL, `Collaborators` longtext COLLATE utf8_bin NOT NULL, `Sex` varchar(10) COLLATE utf8_bin NOT NULL, `Age` varchar(100) COLLATE utf8_bin NOT NULL, `Phases` varchar(15) COLLATE utf8_bin NOT NULL, `Enrollment` int(11) DEFAULT NULL, `Funder_Type` varchar(25) COLLATE utf8_bin NOT NULL, `Study_Type` varchar(15) COLLATE utf8_bin NOT NULL, `Study_Design` varchar(200) COLLATE utf8_bin NOT NULL, `Other_IDs` varchar(200) COLLATE utf8_bin NOT NULL, `Start_Date` date DEFAULT NULL, `Primary_Completion_Date` date DEFAULT NULL, `Completion_Date` date DEFAULT NULL, `First_Posted` date DEFAULT NULL, `Results_First_Posted` date DEFAULT NULL, `Last_Update_Posted` date DEFAULT NULL, `Locations` longtext COLLATE utf8_bin NOT NULL, `Study_Documents` varchar(200) COLLATE utf8_bin NOT NULL ) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin;");

    if ( $stmt-> execute() ) {
	echo "<p>Trials table created &#x2713;</p>";
    } else {
	echo "<p>Error making Trials table</p>";
    }
    
    $dbh = null;
    
}
catch (PDOException $e) {
    
    echo $e->getMessage();
    
}

try { // add ctviewers table
    $dbh = new PDO('mysql:dbname=' . $_POST['dbname'] . ';host=' . $_POST['dbhost'], $_POST['dbusername'], $_POST['dbpassword'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $stmt = $dbh->prepare("DROP TABLE IF EXISTS `ctviewers`; CREATE TABLE `ctviewers` ( `id` int(11) NOT NULL, `address` varchar(32) COLLATE latin1_general_ci NOT NULL, `query` varchar(500) COLLATE latin1_general_ci NOT NULL, `indication` varchar(500) COLLATE latin1_general_ci DEFAULT NULL, `paeds` int(11) NOT NULL DEFAULT '0', `searchdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP, `show_fda_dates` int(11) NOT NULL DEFAULT '1', `colour_by` varchar(10) COLLATE latin1_general_ci NOT NULL DEFAULT 'sponsor', `enrol_view` int(11) NOT NULL DEFAULT '0', `sort_by` varchar(20) COLLATE latin1_general_ci NOT NULL DEFAULT 'ctvSortStartDate', `password` varchar(64) COLLATE latin1_general_ci NOT NULL ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;");

    if ( $stmt-> execute() ) {
	echo "<p>Viewers table created &#x2713;</p>";
    } else {
	echo "<p>Error making Viewers table</p>";
    }
    
    $dbh = null;
    
}
catch (PDOException $e) {
    
    echo $e->getMessage();
    
}

try { // add fda_ActionTypes_Lookup table
    $dbh = new PDO('mysql:dbname=' . $_POST['dbname'] . ';host=' . $_POST['dbhost'], $_POST['dbusername'], $_POST['dbpassword'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $stmt = $dbh->prepare("DROP TABLE IF EXISTS `fda_ActionTypes_Lookup`; CREATE TABLE `fda_ActionTypes_Lookup` ( `ActionTypes_LookupID` int(11) NOT NULL, `ActionTypes_LookupDescription` varchar(100) COLLATE latin1_general_ci NOT NULL, `SupplCategoryLevel1Code` varchar(100) COLLATE latin1_general_ci NOT NULL, `SupplCategoryLevel2Code` varchar(100) COLLATE latin1_general_ci NOT NULL ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;");

    if ( $stmt-> execute() ) {
	echo "<p>FDA Action Types table created &#x2713;</p>";
    } else {
	echo "<p>Error making FDA Action Types table</p>";
    }
    
    $dbh = null;
    
}
catch (PDOException $e) {
    
    echo $e->getMessage();
    
}

try { // add fda_ApplicationDocs table
    $dbh = new PDO('mysql:dbname=' . $_POST['dbname'] . ';host=' . $_POST['dbhost'], $_POST['dbusername'], $_POST['dbpassword'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $stmt = $dbh->prepare("CREATE TABLE `fda_ApplicationDocs` (`ApplicationDocsID` int(11) NOT NULL, `ApplicationDocsTypeID` int(11) NOT NULL, `ApplNo` char(6) COLLATE latin1_general_ci NOT NULL, `SubmissionType` char(6) COLLATE latin1_general_ci NOT NULL, `SubmissionNo` int(11) NOT NULL, `ApplicationDocsTitle` varchar(100) COLLATE latin1_general_ci DEFAULT NULL, `ApplicationDocsURL` varchar(200) COLLATE latin1_general_ci DEFAULT NULL, `ApplicationDocsDate` datetime DEFAULT NULL) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;");

    if ( $stmt-> execute() ) {
	echo "<p>FDA Application Docs table created &#x2713;</p>";
    } else {
	echo "<p>Error making FDA Application Docs table</p>";
    }
    
    $dbh = null;
    
}
catch (PDOException $e) {
    
    echo $e->getMessage();
    
}

try { // add fda_Applications table
    $dbh = new PDO('mysql:dbname=' . $_POST['dbname'] . ';host=' . $_POST['dbhost'], $_POST['dbusername'], $_POST['dbpassword'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $stmt = $dbh->prepare("DROP TABLE IF EXISTS `fda_Applications`; CREATE TABLE `fda_Applications` ( `ApplNo` char(6) COLLATE latin1_general_ci NOT NULL, `ApplType` char(5) COLLATE latin1_general_ci NOT NULL, `ApplPublicNotes` text COLLATE latin1_general_ci, `SponsorName` text COLLATE latin1_general_ci ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;");

    if ( $stmt-> execute() ) {
	echo "<p>FDA Applications table created &#x2713;</p>";
    } else {
	echo "<p>Error making FDA Applications table</p>";
    }
    
    $dbh = null;
    
}
catch (PDOException $e) {
    
    echo $e->getMessage();
    
}

try { // add fda_ApplicationsDocsType table
    $dbh = new PDO('mysql:dbname=' . $_POST['dbname'] . ';host=' . $_POST['dbhost'], $_POST['dbusername'], $_POST['dbpassword'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $stmt = $dbh->prepare("DROP TABLE IF EXISTS `fda_ApplicationsDocsType_Lookup`; CREATE TABLE `fda_ApplicationsDocsType_Lookup` ( `ApplicationDocsType_Lookup_ID` int(11) NOT NULL, `ApplicationDocsType_Lookup_Description` varchar(200) COLLATE latin1_general_ci NOT NULL ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;");

    if ( $stmt-> execute() ) {
	echo "<p>FDA Applications Docs Type Lookup table created &#x2713;</p>";
    } else {
	echo "<p>Error making FDA Applications Docs Type Lookup table</p>";
    }
    
    $dbh = null;
    
}
catch (PDOException $e) {
    
    echo $e->getMessage();
    
}

try { // add fda_MarketingStatus table
    $dbh = new PDO('mysql:dbname=' . $_POST['dbname'] . ';host=' . $_POST['dbhost'], $_POST['dbusername'], $_POST['dbpassword'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $stmt = $dbh->prepare("DROP TABLE IF EXISTS `fda_MarketingStatus`; CREATE TABLE `fda_MarketingStatus` ( `ApplNo` char(6) COLLATE latin1_general_ci NOT NULL, `ProductNo` char(3) COLLATE latin1_general_ci NOT NULL, `MarketingStatusID` int(11) NOT NULL ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;");

    if ( $stmt-> execute() ) {
	echo "<p>FDA Marketing Status table created &#x2713;</p>";
    } else {
	echo "<p>Error making FDA Marketing Status table</p>";
    }
    
    $dbh = null;
    
}
catch (PDOException $e) {
    
    echo $e->getMessage();
    
}

try { // add fda_MarketingStatus_Lookup table
    $dbh = new PDO('mysql:dbname=' . $_POST['dbname'] . ';host=' . $_POST['dbhost'], $_POST['dbusername'], $_POST['dbpassword'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $stmt = $dbh->prepare("DROP TABLE IF EXISTS `fda_MarketingStatus_Lookup`; CREATE TABLE `fda_MarketingStatus_Lookup` ( `MarketingStatusID` int(11) NOT NULL, `MarketingStatusDescription` varchar(200) COLLATE latin1_general_ci NOT NULL ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;");

    if ( $stmt-> execute() ) {
	echo "<p>FDA Marketing Status Lookup table created &#x2713;</p>";
    } else {
	echo "<p>Error making FDA Marketing Status Lookup table</p>";
    }
    
    $dbh = null;
    
}
catch (PDOException $e) {
    
    echo $e->getMessage();
    
}

try { // add fda_Products table
    $dbh = new PDO('mysql:dbname=' . $_POST['dbname'] . ';host=' . $_POST['dbhost'], $_POST['dbusername'], $_POST['dbpassword'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $stmt = $dbh->prepare("DROP TABLE IF EXISTS `fda_Products`; CREATE TABLE `fda_Products` ( `ApplNo` char(6) COLLATE latin1_general_ci NOT NULL, `ProductNo` char(6) COLLATE latin1_general_ci NOT NULL, `Form` varchar(255) COLLATE latin1_general_ci DEFAULT NULL, `Strength` varchar(240) COLLATE latin1_general_ci DEFAULT NULL, `ReferenceDrug` int(11) DEFAULT NULL, `DrugName` varchar(125) COLLATE latin1_general_ci DEFAULT NULL, `ActiveIngredient` varchar(255) COLLATE latin1_general_ci DEFAULT NULL, `ReferenceStandard` int(11) DEFAULT NULL ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;");

    if ( $stmt-> execute() ) {
	echo "<p>FDA Products table created &#x2713;</p>";
    } else {
	echo "<p>Error making FDA Products table</p>";
    }
    
    $dbh = null;
    
}
catch (PDOException $e) {
    
    echo $e->getMessage();
    
}

try { // add fda_SubmissionClass_Lookup table
    $dbh = new PDO('mysql:dbname=' . $_POST['dbname'] . ';host=' . $_POST['dbhost'], $_POST['dbusername'], $_POST['dbpassword'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $stmt = $dbh->prepare("DROP TABLE IF EXISTS `fda_SubmissionClass_Lookup`; CREATE TABLE `fda_SubmissionClass_Lookup` ( `SubmissionClassCodeID` int(11) NOT NULL, `SubmissionClassCode` varchar(50) COLLATE latin1_general_ci NOT NULL, `SubmissionClassCodeDescription` varchar(500) COLLATE latin1_general_ci DEFAULT NULL ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;");

    if ( $stmt-> execute() ) {
	echo "<p>FDA Submission Class Lookup table created &#x2713;</p>";
    } else {
	echo "<p>Error making FDA Submission Class Lookup table</p>";
    }
    
    $dbh = null;
    
}
catch (PDOException $e) {
    
    echo $e->getMessage();
    
}

try { // add fda_SubmissionPropertyType table
    $dbh = new PDO('mysql:dbname=' . $_POST['dbname'] . ';host=' . $_POST['dbhost'], $_POST['dbusername'], $_POST['dbpassword'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $stmt = $dbh->prepare("DROP TABLE IF EXISTS `fda_SubmissionPropertyType`; CREATE TABLE `fda_SubmissionPropertyType` ( `ApplNo` char(6) COLLATE latin1_general_ci NOT NULL, `SubmissionType` char(10) COLLATE latin1_general_ci NOT NULL, `SubmissionNo` int(11) NOT NULL, `SubmissionPropertyTypeCode` varchar(50) COLLATE latin1_general_ci NOT NULL, `SubmissionPropertyTypeID` int(11) NOT NULL ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;");

    if ( $stmt-> execute() ) {
	echo "<p>FDA Submission Property Type table created &#x2713;</p>";
    } else {
	echo "<p>Error making FDA Submission Property Type table</p>";
    }
    
    $dbh = null;
    
}
catch (PDOException $e) {
    
    echo $e->getMessage();
    
}

try { // add fda_Submissions table
    $dbh = new PDO('mysql:dbname=' . $_POST['dbname'] . ';host=' . $_POST['dbhost'], $_POST['dbusername'], $_POST['dbpassword'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $stmt = $dbh->prepare("DROP TABLE IF EXISTS `fda_Submissions`; CREATE TABLE `fda_Submissions` ( `ApplNo` char(6) COLLATE latin1_general_ci NOT NULL, `SubmissionClassCodeID` int(11) DEFAULT NULL, `SubmissionType` char(10) COLLATE latin1_general_ci NOT NULL, `SubmissionNo` int(11) NOT NULL, `SubmissionStatus` char(2) COLLATE latin1_general_ci DEFAULT NULL, `SubmissionStatusDate` datetime DEFAULT NULL, `SubmissionsPublicNotes` text COLLATE latin1_general_ci, `ReviewPriority` varchar(20) COLLATE latin1_general_ci DEFAULT NULL ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;");

    if ( $stmt-> execute() ) {
	echo "<p>FDA Submissions table created &#x2713;</p>";
    } else {
	echo "<p>Error making FDA Submissions table</p>";
    }
    
    $dbh = null;
    
}
catch (PDOException $e) {
    
    echo $e->getMessage();
    
}

try { // add fda_TE table
    $dbh = new PDO('mysql:dbname=' . $_POST['dbname'] . ';host=' . $_POST['dbhost'], $_POST['dbusername'], $_POST['dbpassword'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $stmt = $dbh->prepare("DROP TABLE IF EXISTS `fda_TE`; CREATE TABLE `fda_TE` ( `ApplNo` char(6) COLLATE latin1_general_ci NOT NULL, `ProductNo` char(3) COLLATE latin1_general_ci NOT NULL, `MarketingStatusID` int(11) NOT NULL, `TECode` varchar(100) COLLATE latin1_general_ci NOT NULL ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;");

    if ( $stmt-> execute() ) {
	echo "<p>FDA TE table created &#x2713;</p>";
    } else {
	echo "<p>Error making FDA TE table</p>";
    }
    
    $dbh = null;
    
}
catch (PDOException $e) {
    
    echo $e->getMessage();
    
}

try { // add pmc_cmt_status table
    $dbh = new PDO('mysql:dbname=' . $_POST['dbname'] . ';host=' . $_POST['dbhost'], $_POST['dbusername'], $_POST['dbpassword'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $stmt = $dbh->prepare("DROP TABLE IF EXISTS `pmc_cmt_status`; CREATE TABLE `pmc_cmt_status` ( `CMT_Status` varchar(25) COLLATE latin1_general_ci NOT NULL, `Status_Desc` varchar(30) COLLATE latin1_general_ci DEFAULT NULL, `SortOrder` int(11) DEFAULT NULL ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;");

    if ( $stmt-> execute() ) {
	echo "<p>PMC Commitment Status table created &#x2713;</p>";
    } else {
	echo "<p>Error making PMC Commitment Status table</p>";
    }
    
    $dbh = null;
    
}
catch (PDOException $e) {
    
    echo $e->getMessage();
    
}

try { // add pmc_commitments table
    $dbh = new PDO('mysql:dbname=' . $_POST['dbname'] . ';host=' . $_POST['dbhost'], $_POST['dbusername'], $_POST['dbpassword'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $stmt = $dbh->prepare("DROP TABLE IF EXISTS `pmc_commitments`; CREATE TABLE `pmc_commitments` ( `CMT_ID` int(11) NOT NULL, `CMT_NUMBER` int(11) DEFAULT NULL, `CMT_DOC_TYPE` varchar(5) COLLATE latin1_general_ci DEFAULT NULL, `CMT_DOC_TYPE_NO` varchar(5) COLLATE latin1_general_ci DEFAULT NULL, `CMT_DESC` text COLLATE latin1_general_ci, `CMT_STATUS` varchar(1) COLLATE latin1_general_ci DEFAULT NULL, `CMT_STATUS_DESC` varchar(2500) COLLATE latin1_general_ci DEFAULT NULL, `STUDY_TYPE` varchar(50) COLLATE latin1_general_ci DEFAULT NULL, `STUDY_START_DATE` datetime DEFAULT NULL, `PROTOCOL_SUBMISSION_DATE` datetime DEFAULT NULL, `FINAL_RPT_RECV_DATE` datetime DEFAULT NULL, `ANNUAL_RPT_DUE_DATE` datetime DEFAULT NULL, `ANNUAL_RPT_RECV_DATE` datetime DEFAULT NULL, `NDA_BLA_APPROVAL_DATE` datetime DEFAULT NULL, `ORIG_PROJ_COMPL_DATE` datetime DEFAULT NULL, `CURRENT_PROJ_COMPL_DATE` datetime DEFAULT NULL, `NDA_NUMBER` int(11) DEFAULT NULL, `APPLICANT` varchar(250) COLLATE latin1_general_ci DEFAULT NULL, `PRODUCT` varchar(250) COLLATE latin1_general_ci DEFAULT NULL, `PUBLIC_FLAG` varchar(1) COLLATE latin1_general_ci DEFAULT NULL, `CDER_OR_CBER` varchar(2) COLLATE latin1_general_ci DEFAULT NULL, `SUBPART_FLAG` varchar(1) COLLATE latin1_general_ci DEFAULT NULL ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;");

    if ( $stmt-> execute() ) {
	echo "<p>PMC Commitments table created &#x2713;</p>";
    } else {
	echo "<p>Error making PMC Commitments table</p>";
    }
    
    $dbh = null;
    
}
catch (PDOException $e) {
    
    echo $e->getMessage();
    
}

try { // add devices_pma table
    $dbh = new PDO('mysql:dbname=' . $_POST['dbname'] . ';host=' . $_POST['dbhost'], $_POST['dbusername'], $_POST['dbpassword'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $stmt = $dbh->prepare("DROP TABLE IF EXISTS `devices_pma`; CREATE TABLE `devices_pma` ( `PMANUMBER` varchar(8) COLLATE latin1_general_ci NOT NULL, `SUPPLEMENTNUMBER` varchar(10) NOT NULL, `APPLICANT` varchar(100) NOT NULL, `STREET_1` varchar(50) NOT NULL, `STREET_2` varchar(50) NOT NULL, `CITY` varchar(50) NOT NULL, `STATE` varchar(2) NOT NULL, `ZIP` varchar(5) NOT NULL, `ZIP_EXT` varchar(4) NOT NULL, `GENERICNAME` varchar(150) NOT NULL, `TRADENAME` varchar(150) NOT NULL, `PRODUCTCODE` varchar(3) NOT NULL, `ADVISORYCOMMITTEE` varchar(2) NOT NULL, `SUPPLEMENTTYPE` varchar(50) NOT NULL, `SUPPLEMENTREASON` varchar(50) NOT NULL, `REVIEWGRANTEDYN` varchar(1) NOT NULL, `DATERECEIVED` date NOT NULL, `DECISIONDATE` date NOT NULL, `DOCKETNUMBER` varchar(10) NOT NULL, `FEDREGNOTICEDATE` date NOT NULL, `DECISIONCODE` varchar(4) NOT NULL, `AOSTATEMENT` varchar(500) NOT NULL ) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;");
}
catch (PDOException $e) {
    
    echo $e->getMessage();
    
}
// add the indices for the tables

try {
    $dbh = new PDO('mysql:dbname=' . $_POST['dbname'] . ';host=' . $_POST['dbhost'], $_POST['dbusername'], $_POST['dbpassword'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $stmt = $dbh->prepare("ALTER TABLE `ctg_trials` ADD PRIMARY KEY (`id`), ADD KEY `address` (`address`); ALTER TABLE `ctviewers` ADD PRIMARY KEY (`id`); ALTER TABLE `fda_ActionTypes_Lookup` ADD PRIMARY KEY (`ActionTypes_LookupID`); ALTER TABLE `fda_ApplicationDocs` ADD PRIMARY KEY (`ApplicationDocsID`); ALTER TABLE `fda_ApplicationsDocsType_Lookup` ADD PRIMARY KEY (`ApplicationDocsType_Lookup_ID`); ALTER TABLE `fda_MarketingStatus_Lookup` ADD PRIMARY KEY (`MarketingStatusID`); ALTER TABLE `fda_SubmissionClass_Lookup` ADD PRIMARY KEY (`SubmissionClassCodeID`); ALTER TABLE `ctg_trials` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT; ALTER TABLE `ctviewers` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;");

    if ( $stmt-> execute() ) {
	echo "<p>Table indices altered &#x2713;</p>";
    } else {
	echo "<p>Error altering table indices</p>";
    }
    
    $dbh = null;
    
}
catch (PDOException $e) {
    
    echo $e->getMessage();
    
}

// add auto-increments

try {
    $dbh = new PDO('mysql:dbname=' . $_POST['dbname'] . ';host=' . $_POST['dbhost'], $_POST['dbusername'], $_POST['dbpassword'], array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
    $stmt = $dbh->prepare("ALTER TABLE `ctg_trials` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT; ALTER TABLE `ctviewers` MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;");

    if ( $stmt-> execute() ) {
	echo "<p>Table auto-increment settings altered &#x2713;</p>";
    } else {
	echo "<p>Error altering table auto-increment settings</p>";
    }
    
    $dbh = null;
}
catch (PDOException $e) {
    echo $e->getMessage();
}


// delete the installer files and directory

unlink ( $_POST['abs_path'] . "install/install.php");
unlink ( $_POST['abs_path'] . "install/testmysql.php");
unlink ( $_POST['abs_path'] . "install/writeconfig.php");

if ( ! rmdir ( $_POST['abs_path'] . "install/") ) {
    echo "<p>Installation directory deleted &#x2713;</p>";
}

// make dl directory

if ( ! file_exists ( $_POST['abs_path'] . "dl/" ) ) {

    if ( mkdir ( $_POST['abs_path'] . "dl/" ) ) {

	echo "<p>Downloads directory created &#x2713;</p>";

    } else {

	echo "<p>Error creating downloads directory</p>";

    }

}

include_once ($_POST['abs_path'] . "config.php");

include (ABS_PATH . "cron.php");

?><p><a href="<?php echo $_POST['site_url']; ?>">Clinical Trials Viewer has been installed!</a></p>
