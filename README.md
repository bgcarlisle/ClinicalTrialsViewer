# Clinical trials viewer

## Introduction

The development of a new drug is often depicted as an orderly, linear progression from small, phase 1 trials testing safety to somewhat larger phase 2 trials to generate efficacy hypotheses, and finally larger phase 3 pivotal trials. It is described as a “pipeline,” and even depicted as such in pharmacology lectures and textbooks.

However, the reality of clinical trial activity is much more complicated. For example, a clinical trial does not occur all on a single date, but rather is extended in time, often overlapping with trials in later or earlier phases. Earlier phase trials can sometimes follow higher ones, and intermediary phases can sometimes be skipped altogether. Trial activity often continues after licensure, and grasping the amount of research, along with all the meta-data available can be difficult.

To illustrate the totality of registered clinical trial activity reported on clinicaltrials.gov, the STREAM research group at McGill University has been using *Clinical trials viewer* as an internal research tool since 2012. This software is now available for others to use, install on their own servers, or modify (under the constraint that you make the source code for any modifications available, as per the AGPL).

## Methodology

*Clinical trials viewer* downloads and parses information from clinicaltrials.gov at the time of search to populate the graph of clinical trials. FDA information is updated weekly from the Drugs@FDA dataset and the FDA postmarketing commitment data set.

### For drugs

Yellow flags indicating the dates for FDA submissions appear flattened along the top of the grid. Red flags indicating the dates for FDA documents also appear flattened in the FDA information section. Cyan flags indicating the original projected completion date for FDA postmarketing commitments (PMCs) and requirements (PMRs) also appear here. PMCs and PMRs can be clicked to reveal more details. There are buttons to expand or flatten each of these categories.

### For medical devices

Yellow flags indicating the dates for PMA's appear flattened along the top of the grid. PMA's can be clicked to reveal more details. There is a button to expand or flatten the PMA's.

### Clinical trial information

Below the horizontal rule, there is a graph aligned to the same horizontal date scale indicating the opening and closure dates for clinical trials registered with the NLM clinical trial registry. By default these are sorted by start date, but they can be sorted according to other meta data. Each trial can be clicked to reveal more information.

There are two boxes at the left. The top box contains buttons for customizing the display of information. The bottom box contains a table for all the products found in the FDA database that match the search term provided, sorted by NDA. This application number will also appear on all the FDA submissions, documents and PMCs/PMRs.

### Data sources

Clinical trial data are downloaded at the time of search from ClinicalTrials.gov. The set of clinical trials that are displayed should be the exact same set that you would see if you searched using the "intervention/treatment" field on the advanced search on ClinicalTrials.gov.

FDA submissions, FDA documents, post-marketing commitment data and medical device PMA's are downloaded by a script that keeps MySQL tables corresponding to these data sets up to date. On my personal server, there is a cron job that runs this script once per month.

| Data type       | Source                                                                                                                                                                                 |
|-----------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Clinical trials | [ClinicalTrials.gov](https://www.clinicaltrials.gov "https://www.clinicaltrials.gov")                                                                                                  |
| FDA submissions | [Drugs@FDA](https://www.fda.gov/drugs/drug-approvals-and-databases/drugsfda-data-files "Drugs@FDA")                                                                                    |
| FDA documents   | [Drugs@FDA](https://www.fda.gov/drugs/drug-approvals-and-databases/drugsfda-data-files "Drugs@FDA")                                                                                    |
| PMC's           | [Postmarketing Commitments Data File](https://www.fda.gov/drugs/postmarket-requirements-and-commitments/postmarketing-requirements-and-commitments-downloadable-database-file "PMC's") |
| PMA's           | [FDA medical device PMA data file](https://www.fda.gov/medical-devices/device-approvals-denials-and-clearances/pma-approvals#pma "PMA's")                                              |
|                 |                                                                                                                                                                                        |

## How to use it?

Visit [trials.bgcarlisle.com](https://trials.bgcarlisle.com "trials.bgcarlisle.com") for a live version of *Clinical trials viewer*.

Type the name of a drug into the search field and press enter or click the search button. This will bring up a graph of clinical trial data retrieved from clinicaltrials.gov, along with FDA submissions, FDA documents and postmarketing commitments and requirements.

## Can I install in on my own server?

Yes, and if you intend to be using it a lot, I recommend that you do, so that you don’t crash mine! I have provided the source code, free of charge, and licensed it under the AGPL. This means that anyone can use my code, however, if you build anything on it, or make any modifications to the code, you are obliged to publish your changes.

## Known issues

> When searching for certain drugs, e.g. "vigabatrin," the plot begins at the year 1900.

This is because of a data entry error on the part of the Drugs@FDA database. You can read more about it [here](https://blog.bgcarlisle.com/2019/07/18/most-pediatric-approval-documents-are-filed-under-the-wrong-date-in-the-drugsfda-data-files/ "Most pediatric approval documents are filed under the wrong date in the Drugs@FDA data files").

## Acknowledgements

*Clinical trials viewer* was built for installation on a LAMP stack using Bootstrap v 4.3.1, and jQuery v 3.4.1.

*Clinical trials viewer* draws data to populate its graphs from the following sources: clinicaltrials.gov, Drugs@FDA, FDA PMC Download.

*Clinical trials viewer* was originally designed for use by the STREAM research group at McGill University in Montreal Canada to work on the Signals, Safety and Success CIHR grant.

## Citing Clinical trials viewer

Here is a BibTeX entry for *Clinical trials viewer*:

```
@Manual{bgcarlisle-ClinicalTrialsViewer,
  Title          = {Clinical trials viewer},
  Author         = {Carlisle, Benjamin Gregory},
  Organization   = {The Grey Literature},
  Address        = {Berlin, Germany},
  url            = {https://codeberg.org/bgcarlisle/ClinicalTrialsViewer},
  year           = 2020
}
```

You may also cite this resource as: (Clinical trials viewer, RRID:SCR_019230).

If you used this in your research and you found it useful, I would take it as a kindness if you cited it.

Best,

Benjamin Gregory Carlisle PhD
