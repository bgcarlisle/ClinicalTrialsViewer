<?php

$ctviewer = ctv_get_ctviewer_for_address ( $_GET['address'] );

$firststart = ctv_get_first_start_for_address ( $_GET['address'], $ctviewer );
$lastend = ctv_get_last_end_for_address ( $_GET['address'], $ctviewer );
$viewerwidth = ctv_get_width_of_display ($firststart, $lastend);

$ctgtrials = ctv_get_trials_for_address ( $_GET['address'] );

$fdaapps = ctv_get_applications_for_query ( $ctviewer['query'] );

if ( count ($fdaapps) > 0 ) {
    $fdaproducts = ctv_get_apps_and_products_for_query ($ctviewer['query']);
    // $pmcs = ctv_get_apps_and_pmcs_for_query ($ctviewer['query']);
    $fdasubs = ctv_get_apps_and_subs_for_query ( $ctviewer['query'] );
    $fdadocs = ctv_get_apps_and_docs_for_query ( $ctviewer['query'] );
}

$devices = ctv_get_devices_for_query ($ctviewer['query']);

// echo "devices: [" . count($devices) . "]";

?><div style="position: fixed; margin-top: 30px; z-index: 700;">
    <div class="ctvSticky">
	<h1><a href="<?php echo SITE_URL; echo $ctviewer['address']; ?>/">Search term: <?php echo urldecode ($ctviewer['query']); ?></a></h1>
	<?php
	if (! is_null ($ctviewer['indication'])) {

	    echo "<p>Indication: " . urldecode ($ctviewer['indication']) . "</p>";
	    
	}
	?>
	<p><?php echo count ( $ctgtrials ); ?> record(s) found on clinicaltrials.gov</p>
	<?php if ( count ($fdaapps) > 0 ) { ?>
	    <p>Expand or flatten:</p>
	    <div class="btn-group" role="group" aria-label="FDA info">
		<button type="button" class="btn btn-primary" onclick="$('.fdaSubmission').toggleClass('fdaSubmissionFlat').toggleClass('fdaSubmissionStaggered');">FDA submissions</button>
		<button type="button" class="btn btn-primary" onclick="$('.fdaDocument').toggleClass('fdaDocumentFlat').toggleClass('fdaDocumentStaggered');">FDA docs</button>
		<!-- <button type="button" class="btn btn-primary" onclick="$('.fdaPMC').toggleClass('fdaPMCFlat').toggleClass('fdaPMCStaggered');">PMCs</button> -->
	    </div>

	    <p>Yellow: FDA submissions; red: FDA documents<!-- ; cyan: postmarketing commitments (PMCs)--></p>
	<?php } ?>
	<?php if ( count ($devices) > 0 ) { ?>
	    <div class="btn-group" role="group" aria-label="Medical device PMAs">
		<button type="button" class="btn btn-primary" onclick="$('.devicePMA').toggleClass('devicePMAFlat').toggleClass('devicePMAStaggered');">Expand/flatten PMAs</button>
	    </div>

	    <p>Yellow: medical device premarketing approvals (PMAs)</p>
	<?php } ?>
	<label for="sortby">Sort trials by</label>
	<div class="btn-group btn-group-sm btn-block" id="sortby" role="group" aria-label="Sort by">
	    <button type="button" id="ctvSortStartDateButton" class="btn btn-primary" onclick="ctv_sort_trials(event, $(this), 'ctvSortStartDate');"<?php
																		    if ($ctviewer['sort_by'] == "ctvSortStartDate") {
																			echo " disabled";
																		    }
																		    ?>>
		Start
	    </button>
	    <button type="button" id="ctvSortEndDateButton" class="btn btn-primary" onclick="ctv_sort_trials(event, $(this), 'ctvSortEndDate');"<?php
																		if ($ctviewer['sort_by'] == "ctvSortEndDate") {
																		    echo " disabled";
																		}
																		?>>
		End
	    </button>
	    <button type="button" id="ctvSortPhaseButton" class="btn btn-primary" onclick="ctv_sort_trials(event, $(this), 'ctvSortPhase');"<?php
																	    if ($ctviewer['sort_by'] == "ctvSortPhase") {
																		echo " disabled";
																	    }
																	    ?>>
		Phase
	    </button>
	    <button type="button" id="ctvSortFundingButton" class="btn btn-primary" onclick="ctv_sort_trials(event, $(this), 'ctvSortFunding');"<?php
																		if ($ctviewer['sort_by'] == "ctvSortFunding") {
																		    echo " disabled";
																		}
																		?>>
		Funding
	    </button>
	</div>
	<!-- <label for="colourby">Colour by</label>
	     <div class="btn-group btn-group-sm btn-block" id="colourby" role="group" aria-label="Colour by">
	     <button type="button" class="btn btn-primary" disabled>None</button>
	     <button type="button" class="btn btn-primary">Phase</button>
	     <button type="button" class="btn btn-primary">Industry</button>
	     <button type="button" class="btn btn-primary">Type</button>
	     <button type="button" class="btn btn-primary">Gender</button>
	     </div> -->
    </div>
    <?php if ( count ($fdaapps) > 0 ) { ?>
	<div class="ctvSticky" style="position: fixed; overflow-y: scroll; height: 500px;">
	    <p><?php echo count ($fdaapps); ?> FDA application(s) found for this search term</p>
	    <table class="table table-dark table-hover table-sm">
		<thead>
		    <th scope="col">#</th>
		    <th scope="col">Product</th>
		    <th scope="col">Sponsor</th>
		    <th scope="col">Events</th>
		</thead>
		<tbody>
		    <?php

		    foreach ($fdaapps as $fdaapp) {

			echo "<tr class=\"table-info\">";
			echo "<td>" . $fdaapp['ApplType'] . " " . $fdaapp['ApplNo'] . "</td>";
			echo "<td>" . $fdaapp['DrugName'] . " (" . $fdaapp['ActiveIngredient'] . ")</td>";
			echo "<td>" . $fdaapp['SponsorName'] . "</td>";
			echo "<td>" . count (ctv_get_submissions_for_product ($fdaapp['ApplNo'])) . " submissions(s)<br>";
			echo count (ctv_get_documents_for_product ($fdaapp['ApplNo'])) . " document(s)</td>";
			echo "</tr>";
			
			// $fdaproducts = ctv_get_products_for_application ( $fdaapp['ApplNo'] );

			$refstandardfound = 0;

			foreach ($fdaproducts as $fdaproduct) {
			    if ( $fdaproduct['ApplNo'] == $fdaapp['ApplNo'] ) {
				echo "<tr><td colspan=\"4\">";
				echo $fdaproduct['ApplNo'] . "-" . $fdaproduct['ProductNo'];
				if ($fdaproduct['ReferenceStandard'] == 1) {
				    echo "*";
				    $refstandardfound++;
				}
				echo " " . $fdaproduct['Form'] . ", ";
				echo $fdaproduct['Strength'] . "<br>";
				echo "Marketing status: " . $fdaproduct['MarketingStatusDescription'];
				echo "</td></tr>";
			    }
			    
			}

			if ($refstandardfound > 0) {
			    echo "<tr><td colspan=\"4\">Asterisk (*) indicates reference standard</td></tr>";
			}

		    }
		    
		    ?>
		</tbody>
	    </table>
	</div>
    <?php } ?>
    <?php if ( count ($devices) > 0 ) { ?>
	<div class="ctvSticky" style="position: fixed; overflow-y: scroll; height: 500px;">
	    <p><?php echo count ($devices); ?> medical device PMAs found for this search term</p>
	    <table class="table table-dark table-hover table-sm">
		<thead>
		    <th scope="col">#</th>
		    <th scope="col">Device name (trade name)</th>
		    <th scope="col">Applicant</th>
		    <th scope="col">Decision code</th>
		</thead>
		<tbody>
		    <?php

		    foreach ($devices as $device) {

			echo "<tr>";
			echo "<td>" . $device['PMANUMBER'];

			if ($device['SUPPLEMENTNUMBER'] != '') {
			    echo "-" . $device['SUPPLEMENTNUMBER'];
			}
			
			echo "</td>";
			echo "<td>" . $device['GENERICNAME'] . " (" . $device['TRADENAME'] . ")</td>";
			echo "<td>" . $device['APPLICANT'] . "</td>";
			echo "<td>" . $device['DECISIONCODE'] . " (" . $device['DECISIONDATE'] . ")</td>";
			echo "</tr>";

		    }
		    
		    ?>
		</tbody>
	    </table>
	</div>
    <?php } ?>
</div>

<div style="position: fixed; margin-top: 30px; z-index: 700; right: 10px;">
    <?php

    // for each trial, put a hidden div with its details

    foreach ( $ctgtrials as $trial ) {

    ?><div class="ctvSticky ctvTrialDetails ctvHidden" id="ctvDetails-<?php echo $trial['NCT_Number']; ?>">
	<ul class="nav nav-pill nav-fill" style="margin-bottom: 10px;">
	    <li class="nav-item">
		<a class="nav-link" href="#" onclick="event.preventDefault();$('#ctvTrial-<?php echo $trial['NCT_Number']; ?>').prev().click();">Previous (k)</a>
	    </li>
	    <li class="nav-item">
		<a class="nav-link" href="#" onclick="event.preventDefault();hide_trial_details();">Close (c)</a>
	    </li>
	    <li class="nav-item">
		<a class="nav-link" href="#" onclick="event.preventDefault();$('#ctvTrial-<?php echo $trial['NCT_Number']; ?>').next().click();">Next (j)</a>
	    </li>
	</ul>
	<h2><a href="<?php echo $trial['Study_URL']; ?>" target="_blank"><?php echo $trial['NCT_Number']; ?></a>: <?php echo $trial['Title']; ?></h2>
	<p>Other ID(s): <?php echo $trial['Other_IDs']; ?></p>
	<p>Status: <?php echo $trial['Study_Status']; ?></p>
	<p>Phase: <?php echo $trial['Phases']; ?></p>
	<p>Indications: <?php echo $trial['Conditions']; ?></p>
	<p>Age: <?php echo $trial['Age']; ?></p>
	<hr>
	<p>Start: <?php

		  if ( ! is_null ($trial['Start_Date']) ) {
		      if ( substr($trial['Start_Date'],8 ,2) == "00" ) {
			  echo substr($trial['Start_Date'], 0, 7) . "-01";
		      } else {
			  echo $trial['Start_Date'];
		      }
		  } else {
		      echo "NS";
		  }

		  ?></p>
	<p>Primary completion: <?php

			       if ( ! is_null($trial['Primary_Completion_Date']) ) {
				   if ( substr($trial['Primary_Completion_Date'],8 ,2) == "00" ) {
				       echo substr($trial['Primary_Completion_Date'], 0, 7) . "-01";
				   } else {
				       echo $trial['Primary_Completion_Date'];
				   }
			       } else {
				   echo "NS";
			       }

			       ?></p>
	<p>Completion: <?php

		       if ( ! is_null($trial['Completion_Date']) ) {
			   if ( substr($trial['Completion_Date'],8 ,2) == "00" ) {
			       echo substr($trial['Completion_Date'], 0, 7) . "-01";
			   } else {
			       echo $trial['Completion_Date'];
			   }
		       } else {
			   echo "NS";
		       }

		       ?></p>
	<hr>
	<p>Enrolment: <?php

		      if (! is_null($trial['Enrollment'])) {
			  echo $trial['Enrollment'];
		      } else {
			  echo "NS";
		      }

		      ?></p>
	<hr>
	<p>Funding: <?php echo $trial['Funder_Type']; ?></p>
	<p>Sponsor: <?php echo $trial['Sponsor']; ?></p>
    </div>
    <?php
    }

    // For each FDA application, put a hidden div for each postmarketing commitment
    
    if ( count ($fdaapps) > 0 ) {
	
	foreach ($pmcs as $pmc) {

	    echo "<div class=\"ctvSticky ctvTrialDetails ctvHidden\" id=\"ctvDetails-PMC" . $pmc['CMT_ID'] . "\">";
	    echo "<a href=\"#\" onclick=\"event.preventDefault();hide_trial_details();\" style=\"margin: 0 1px 4px 10px; float: right;\">[Close]</a>";
	    echo "<h2>";

	    if ( $pmc['SUBPART_FLAG'] == "" ) {
		echo "Postmarketing commitment ";
	    } else {
		echo "Postmarketing requirement ";
	    }

	    echo $pmc['CMT_ID'] . "-" . $pmc['CMT_NUMBER'] . " [" . $fdaapp['ApplNo'] . "]</h2>";
	    echo "<p>Supplement number: " . $pmc['CMT_DOC_TYPE'] . "-" . $pmc['CMT_DOC_TYPE_NO'] . "</p>";
	    if ( $pmc['SUBPART_FLAG'] != "" ) {
		switch ($pmc['SUBPART_FLAG']) {
		    case "H":
			echo "<p>Required under: <a href=\"http://www.fda.gov/Drugs/GuidanceComplianceRegulatoryInformation/Post-marketingPhaseIVCommitments/ucm070766.htm#q13\" target=\"_blank\">Accelerated approval</a></p>";
			break;

		    case "F":
			echo "<p>Required under: <a href=\"http://www.fda.gov/Drugs/GuidanceComplianceRegulatoryInformation/Post-marketingPhaseIVCommitments/ucm070766.htm#q16\" target=\"_blank\">FDAAA section 505(o)(3)</a></p>";
			break;

		    case "P":
			echo "<p>Required under: <a href=\"http://www.fda.gov/Drugs/GuidanceComplianceRegulatoryInformation/Post-marketingPhaseIVCommitments/ucm070766.htm#q15\" target=\"_blank\">Pediatric research equity act</a></p>";
			break;
			
		    case "E":
			echo "<p>Required under: <a href=\"http://www.fda.gov/Drugs/GuidanceComplianceRegulatoryInformation/Post-marketingPhaseIVCommitments/ucm070766.htm#q14\" target=\"_blank\">Animal efficacy rule</a></p>";
			break;
		}
	    }

	    echo "<p>Current projected completion date: " . substr($pmc['CURRENT_PROJ_COMPL_DATE'], 0, 10 ) . "</p>";
	    
	    if ( $pmc['CMT_STATUS_DESC'] != "" ) {
		echo "<p>Status description: ";
		echo $pmc['CMT_STATUS_DESC'];
		echo "</p>";
	    }

	    echo "<p>Description: " . $pmc['CMT_DESC'] . "</p>";
	    echo "<p>Applicant: " . $pmc['APPLICANT'] . "</p>";
	    echo "<p>Product: " . $pmc['PRODUCT'] . " [" . $pmc['NDA_NUMBER'] . "]</p>";

	    echo "</div>";
	    
	}
	
    }

    // For each medical device PMA, put a hidden div

    if ( count ($devices) > 0 ) {

	foreach ( $devices as $device ) {

	    echo "<div class=\"ctvSticky ctvTrialDetails ctvHidden\" id=\"ctvDetails-PMA-" . $device['PMANUMBER'] . "-" . $device['DECISIONDATE'] . "\">";
	    echo "<a href=\"#\" onclick=\"event.preventDefault();hide_trial_details();\" style=\"margin: 0 1px 4px 10px; float: right;\">[Close]</a>";
	    echo "<h2>PMA number: " . $device['PMANUMBER'] . "</h2>";

	    if ( $device['SUPPLEMENTNUMBER'] != '') {
		echo "<p>Supplement number: " . $device['SUPPLEMENTNUMBER'] . "</p>";
	    }
	    
	    echo "<hr>";

	    echo "<p>Applicant: " . $device['APPLICANT'] . "</p>";
	    echo "<p>Generic name: " . $device['GENERICNAME'] . "</p>";
	    echo "<p>Trade name: " . $device['TRADENAME'] . "</p>";
	    echo "<p>Product code: " . $device['PRODUCTCODE'] . "</p>";
	    echo "<p>Review granted: " . $device['REVIEWGRANTEDYN'] . "</p>";

	    echo "<hr>";
	    
	    echo "<p>Date received: " . $device['DATERECEIVED'] . "</p>";
	    echo "<p>Decision date: " . $device['DECISIONDATE'] . "</p>";
	    echo "<p>Decision code: " . $device['DECISIONCODE'] . "</p>";

	    echo "<hr>";
	    
	    echo "<p>" . $device['AOSTATEMENT'] . "</p>";

	    echo "</div>";
	    
	}
	
    }
    
    ?>
</div>
<div style="border: 1px solid #666; min-height: 200px; height: 100%; margin-top: 50px; margin-left: 350px; width: <?php echo $viewerwidth + 350; ?>px; padding-right: 350px;">
    <div id="ctvDrugViewGrid" style="padding: 40px 0px 10px 0px; background: url('https://trials.bgcarlisle.com/images/timeline.png'); overflow: visible; width: <?php echo $viewerwidth + 350; ?>px; min-height: 200px;">
	<div id="ctvYearMarkerContainer" style="width: <?php echo $viewerwidth; ?>px;">
	    <?php

	    // Label the years on the graph

	    $counter = substr($firststart, 0, 4);

	    while ($counter <= substr($lastend, 0, 4)) {

		echo "<div class=\"ctvYearMarker\">";

		echo $counter;

		echo "</div>";
		
		$counter++;
	    }
	    ?>
	</div>
	<?php

	if ( count ($fdaapps) > 0 ) {
	    echo "<div class=\"ctvGraphSectionLabel\" style=\"margin-bottom: 70px;\">FDA information</div>";
	}
	
	// Put FDA submissions, documents, pmc's and pma's on the graph

	if ( count ( $fdaapps ) > 0 ) {

	    echo "<div id=\"fdaInfoContainer\">";

	    foreach ($fdaapps as $fdaapp) { // Need to optimize ***

		// submissions
		
		// $fdasubs = ctv_get_submissions_for_product ($fdaapp['ApplNo']);

		foreach ( $fdasubs as $fdasub ) {
		    if ( $fdaapp['ApplNo'] == $fdasub['ApplNo'] ) {

			$subleftyears = ( substr($fdasub['SubmissionStatusDate'], 0, 4) - substr($firststart, 0, 4) ) * 120;
			$subleftmonths = substr($fdasub['SubmissionStatusDate'], 5, 2) * 10 - 10;
			$subleft = $subleftyears + $subleftmonths;

			echo "<div class=\"fdaSubmission fdaSubmissionFlat\" style=\"left: ";
			
			echo $subleft;
			echo "px;\" fdaSortDate=\"";
			echo $fdasub['ApplNo'] . "-" . $fdasub['SubmissionStatusDate'] . "-1\">";
			echo $fdasub['SubmissionType'] . "-" . $fdasub['SubmissionNo'];
			echo " (";
			echo $fdasub['ReviewPriority'] . " " . $fdasub['SubmissionClassCode'] . " " . $fdasub['SubmissionClassCodeDescription'];
			echo ")<br>";

			echo "<span class=\"fdaDocApplNo\">" . $fdasub['ApplNo'] . "</span>";
			// echo "-";
			// echo "<span class=\"fdaProdNo\">" . $fdasub['ProductNo'] . "</span>";
			echo "</div>";
			
		    }
		}

		// documents

		// $fdadocs = ctv_get_documents_for_product ($fdaapp['ApplNo']);

		foreach ($fdadocs as $fdadoc) {

		    if ( $fdaapp['ApplNo'] == $fdadoc['ApplNo'] ) {

			$docleftyears = ( substr($fdadoc['ApplicationDocsDate'], 0, 4) - substr($firststart, 0, 4) ) * 120;
			$docleftmonths = substr($fdadoc['ApplicationDocsDate'], 5, 2) * 10 - 10;
			$docleft = $docleftyears + $docleftmonths;

			echo "<div class=\"fdaDocument fdaDocumentFlat\" style=\"left: " . $docleft . "px;\" fdaSortDate=\"" . $fdasub['ApplNo'] . "-" . $fdadoc['ApplicationDocsDate'] . "-2\">";
			echo "<a href=\"" . $fdadoc['ApplicationDocsURL'] . "\" target=\"_blank\">";
			echo $fdadoc['ApplicationDocsType_Lookup_Description'] . " (" . $fdadoc['SubmissionType'] . "-" . $fdadoc['SubmissionNo'] . ")";
			echo "</a>";
			echo "<br>";
			echo "<span class=\"fdaDocApplNo\">" . $fdadoc['ApplNo'] . "</span>";
			echo "</div>";
			
		    }
		    
		}

		// postmarketing commitments

		foreach ($pmcs as $pmc) {

		    if ( $fdaapp['ApplNo'] == $pmc['NDA_NUMBER']) {

			echo "YES " . $pmc['NDA_NUMBER'];

			$pmcleftyears = ( substr($pmc['CURRENT_PROJ_COMPL_DATE'], 0, 4) - substr($firststart, 0, 4) ) * 120;
			$pmcleftmonths = substr($pmc['CURRENT_PROJ_COMPL_DATE'], 5, 2) * 10 - 10;
			$pmcleft = $pmcleftyears + $pmcleftmonths;

			echo "<div class=\"fdaPMC fdaPMCFlat\" style=\"left: " . $pmcleft . "px;\" fdaSortDate=\"" . $fdaapp['ApplNo'] . "-" . $pmc['CURRENT_PROJ_COMPL_DATE'] ."-3\" onclick=\"show_trial_details('PMC" . $pmc['CMT_ID'] . "');\">";

			if ( $pmc['SUBPART_FLAG'] == "" ) {
			    echo "Postmarketing commitment ";
			} else {
			    echo "Postmarketing requirement ";
			}

			echo $pmc['CMT_ID'] . "-" . $pmc['CMT_NUMBER'] . " / " . $pmc['CMT_DOC_TYPE'] . "-" . $pmc['CMT_DOC_TYPE_NO'] . " (" . $pmc['CMT_STATUS_DESC'] . ")";
			echo "<br>";

			echo "<span class=\"fdaDocApplNo\">" . $fdaapp['ApplNo'] . "</span>";
			
			echo "</div>";
			
		    }

		}
	    }

	    echo "</div><hr>";
	    
	}

	if ( count ($devices) > 0 ) {
	    echo "<div class=\"ctvGraphSectionLabel\" style=\"margin-bottom: 100px;\">Medical device PMAs</div>";
	}

	if ( count ($devices) > 0 ) {

	    echo "<div id=\"fdaInfoContainer\">";

	    foreach ($devices as $device) {
		$pmaleftyears = ( substr($device['DECISIONDATE'], 0, 4) - substr($firststart, 0, 4) ) * 120;
		$pmaleftmonths = substr($device['DECISIONDATE'], 5, 2) * 10 - 10;
		$pmaleft = $pmaleftyears + $pmaleftmonths;

		echo "<div class=\"devicePMA devicePMAFlat\" style=\"left: ";
		
		echo $pmaleft;
		echo "px;\" fdaSortDate=\"";
		echo $device['PMANUMBER'] . "-" . $device['DECISIONDATE'] . "-1\" onclick=\"show_trial_details('PMA-" . $device['PMANUMBER'] . "-" . $device['DECISIONDATE'] . "');\">";
		echo $device['PMANUMBER'] . ": ";
		echo $device['GENERICNAME'] . " (" . $device['TRADENAME'] . ")<br>";
		echo $device['DECISIONCODE'] . " (" . $device['DECISIONDATE'] . ")";
		
		echo "</div>";
		
	    }

	    echo "</div><hr>";

	}
	
	?>
	<div class="ctvGraphSectionLabel" style="margin-bottom: 20px;">Trial records from clinicaltrials.gov</div>
	<?php

	// Now put the trials on the graph

	foreach ( $ctgtrials as $trial ) {
	    
	    // Get the right and left bounds
	    // There are 6 possible cases, numbered below

	    if ( ! is_null ($trial['Start_Date']) ) { // There is a start date

		if ( ! is_null ($trial['Primary_Completion_Date']) ) { // There is a start date and a primary completion date

		    // 1. Make left and right bounded by start and primary completion dates

		    $lmyears = ( substr($trial['Start_Date'], 0, 4) - substr($firststart, 0, 4) ) * 120;
		    $lmmonths = substr($trial['Start_Date'], 5, 2) * 10 - 10;
		    $leftmargin = $lmyears + $lmmonths;

		    $trialwidth = floor ( ( strtotime(substr($trial['Primary_Completion_Date'], 0, 7) . "-01") - strtotime(substr($trial['Start_Date'], 0, 7) . "-01") ) / 60 / 60 / 24 / 365 * 120 );

		    $noDateInfo = "";

		} else { // There is a start date but no primary completion date

		    if ( ! is_null ($trial['Completion_Date']) ) { // There is a start date and a completion date

			// 2. Make left and right bounded by start and completion date

			$lmyears = ( substr($trial['Start_Date'], 0, 4) - substr($firststart, 0, 4) ) * 120;
			$lmmonths = substr($trial['Start_Date'], 5, 2) * 10 - 10;
			$leftmargin = $lmyears + $lmmonths;

			$trialwidth = floor ( ( strtotime(substr($trial['Completion_Date'], 0, 7) . "-01") - strtotime(substr($trial['Start_Date'], 0, 7) . "-01") ) / 60 / 60 / 24 / 365 * 120 );

			$noDateInfo = "";

		    } else { // There is a start date, but no completion date of any kind

			// 3. Make it fade away to the right

			$lmyears = ( substr($trial['Start_Date'], 0, 4) - substr($firststart, 0, 4) ) * 120;
			$lmmonths = substr($trial['Start_Date'], 5, 2) * 10 - 10;
			$leftmargin = $lmyears + $lmmonths;

			$trialwidth = 250;

			$noDateInfo = " NoEndDate";

		    }
		}

	    } else { // No start date

		if ( ! is_null ($trial['Primary_Completion_Date']) ) { // No start date, but there is a primary completion date
		    // 4. Make it fade away to the left

		    $lmyears = ( substr($trial['Primary_Completion_Date'], 0, 4) - substr($firststart, 0, 4) ) * 120;
		    $lmmonths = substr($trial['Primary_Completion_Date'], 5, 2) * 10 - 10;
		    $leftmargin = $lmyears + $lmmonths - 250;

		    $noDateInfo = " NoStartDate";

		} else { // No start date, no primary completion date

		    if ( ! is_null ($trial['Completion_Date']) ) { // No start date, but there is a completion date

			// 5. Make it fade away to the left

			$lmyears = ( substr($trial['Completion_Date'], 0, 4) - substr($firststart, 0, 4) ) * 120;
			$lmmonths = substr($trial['Completion_Date'], 5, 2) * 10 - 10;
			$leftmargin = $lmyears + $lmmonths - 250;

			$noDateInfo = " NoStartDate";

		    } else { // No dates of any kind

			// 6. Make it fade away both ways

			$leftmargin = 0;
			$trialwidth = 250;
			$noDateInfo = " NoDates";

		    }
		}

	    }

	    // Now you have the left and right bounds
	    
	    // Get stuff for colouring (to-do!)

	    echo "<div class=\"ctvTrial" . $noDateInfo . "\" id=\"ctvTrial-" . $trial['NCT_Number'] . "\" style=\"margin-left: " . $leftmargin . "px; width: " . $trialwidth . "px;\" onclick=\"show_trial_details('" . $trial['NCT_Number'] . "');\" ctvSortStartDate=\"" . $trial['Start_Date'] . "\" ctvSortEndDate=\"" . $trial['End_Date'] . "\" ctvSortPhase=\"" . $trial['Phases'] . "-" . $trial['Start_Date'] . "\" ctvSortFunding=\"" . $trial['Funder_Type'] . "-" . $trial['Start_Date'] . "\">";
	    echo $trial['NCT_Number'] . " | " . $trial['Phases'] . " | " . $trial['Conditions'];
	    echo "</div>";
	    
	}

	?>
    </div>
</div>
