<!DOCTYPE html>
<html lang="en">
    <head>
	<meta charset="utf-8">

	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

	<title>Clinical trials viewer</title>

	<!-- Bootstrap -->
	<link href="<?php echo SITE_URL; ?>css/bootstrap.min.css" rel="stylesheet">

	<!-- CTNotebook CSS -->
	<link href="<?php echo SITE_URL; ?>ctviewer.css" rel="stylesheet">

	<meta name="author" content="Benjamin Gregory Carlisle PhD Email: murph@bgcarlisle.com Website: https://www.bgcarlisle.com/ Social media: https://scholar.social/@bgcarlisle">

    </head>
    <body>
	<div class="container-fluid ctviewer-nav" id="nav-container" style="position: fixed; top: 0; z-index: 1000;">
	    <ul class="nav">
		<li class="nav-item">
		  <a class="nav-link active" href="<?php echo SITE_URL; ?>">Clinical trials viewer</a>
		</li>
		<li class="nav-item">
		  <a class="nav-link" href="https://blog.bgcarlisle.com/ctviewer/" target="_blank">About</a>
		</li>
		<li class="nav-item">
		  <a class="nav-link" href="#" onclick="show_citation(event);">Cite</a>
		</li>
		<li class="nav-item">
		  <a class="nav-link" href="https://codeberg.org/bgcarlisle/ClinicalTrialsViewer" target="_blank">Source</a>
		</li>
	    </ul>
	</div>
	<div id="citeMask" class="ctvHidden" onclick="hide_citation (event);">&nbsp;</div>
	<div id="citeCTViewer" class="ctvHidden">
	    <h2>How to cite Clinical trials viewer</h2>
	    <h3>BibTeX</h3>
	    <pre>
@software{carlisle_ctviewer_2019,
location = {{Retrieved from https://trials.bgcarlisle.com/}},
title = {Clinical trials viewer},
url = {https://trials.bgcarlisle.com/},
organization = {{The Grey Literature}},
date = {2019-06},
author = {Carlisle, Benjamin Gregory}
}
	    </pre>
	    <h3>Vancouver</h3>
	    <p>Carlisle BG. Clinical trials viewer [Internet]. Retrieved from https://trials.bgcarlisle.com/: The Grey Literature; 2019. Available from: https://trials.bgcarlisle.com/</p>
	    <h3>AMA</h3>
	    <p>Carlisle BG. <i>Clinical Trials Viewer</i>. Retrieved from https://trials.bgcarlisle.com/: The Grey Literature; 2019. https://trials.bgcarlisle.com/.</p>
            <h3>MLA</h3>
            <p>Carlisle, Benjamin Gregory. <i>Clinical Trials Viewer.</i> The Grey Literature, 2019, https://trials.bgcarlisle.com/.</p>
	    <h3>RRID</h3>
	    <p>You may also cite this resource as: (Clinical trials viewer, RRID:SCR_019230).</p>
            <button onclick="hide_citation(event);" class="btn btn-block btn-primary">Close citation information</button>
        </div>

	<?php

	if ( ! isset ( $_GET['address'] ) ) { // No ctviewer address specified

	    // Show search box

	    include ( ABS_PATH . 'search-field.php' );

	} else {

	    // Show plot

	    include ( ABS_PATH . "plot-viewer.php" );
	}
	
	?>
	
	<!-- jQuery -->
	<script src="<?php echo SITE_URL; ?>jquery-3.4.1.min.js"></script>

	<!-- Popper.js -->
	<script src="<?php echo SITE_URL; ?>js/bootstrap.bundle.min.js"></script>

	<!-- Bootstrap JS -->
	<script src="<?php echo SITE_URL; ?>js/bootstrap.min.js"></script>

	<!-- CTViewer JS -->
	<script>

	 var ctviewerurl = '<?php echo SITE_URL; ?>';

	 function ctviewer_search () {

	     $('#search-form').slideUp(500, function () {
		 $('#search-progress').html('<div style="text-align: center; margin-top: 50px;"><img src="https://trials.bgcarlisle.com/images/loading.gif"><p>Downloading data from <a href="https://www.clinicaltrials.gov/">www.clinicaltrials.gov</a>. This may take a minute or two.</p></div>');
		 $('#search-progress').slideDown();
	     });

	     $.ajax ({
		 url: ctviewerurl + 'query.php',
		 type: 'post',
		 data: {
		     drug: $('#drug-query').val(),
		     indication: $('#indication-query').val(),
		     paeds: $('#paeds-checkbox').prop('checked')
		 },
		 dataType: 'html'
	     }).done ( function (response) {
		 $('#search-progress').html('');
		 if ( response != 'FALSE' ) {
		     window.location.href = ctviewerurl + response + '/';
		 } else {
		     alert ('Could not download trial data for: ' + $('#drug-query').val());
		     window.location.href = ctviewerurl;
		 }

	     });

	 }

	 function show_trial_details ( nct_number ) {
	     $('.ctvTrial').removeClass('selectedTrial');
	     $('.ctvTrialDetails:not(#ctvDetails-' + nct_number + ')').slideUp(400, function () {
		 // This fails in the case that there are no other Trial Details div's
		 $('#ctvDetails-' + nct_number).slideDown();
		 $('#ctvTrial-' + nct_number).addClass('selectedTrial');
	     });
	     // So I put this here for the case where there's only one:
	     if ($('.ctvTrialDetails').length == 1) {
		 $('#ctvDetails-' + nct_number).slideDown();
		 $('#ctvTrial-' + nct_number).addClass('selectedTrial');
	     }
	 }
	 function hide_trial_details () {
	     $('.ctvTrial').removeClass('selectedTrial');
	     $('.ctvTrialDetails').slideUp();
	 }

	 function show_citation (event) {
	     event.preventDefault();
	     $('#citeMask').fadeIn(400, function () {
		 $('#citeCTViewer').slideDown();
	     });
	 }
	 function hide_citation (event) {
	     event.preventDefault();
	     $('#citeCTViewer').slideUp(400, function () {
		 $('#citeMask').fadeOut();
	     });
	 }

	 function ctv_sort_trials (event, button, sortby) {
	     event.preventDefault();

	     $.ajax ({
		 url: ctviewerurl + 'sort.php',
		 type: 'post',
		 data: {
		     sortby: sortby,
		     address: '<?php echo $ctviewer['address']; ?>'
		 },
		 dataType: 'html'
	     }).done ( function (response) {
		 var $trials = $('.ctvTrial');

		 var sorted_trials = $trials.sort(function (a, b) {
		     return $(a).attr(sortby) > $(b).attr(sortby) ? 1 : -1;
		 });

		 $('.ctvTrial').remove();

		 $('div#ctvDrugViewGrid').append(sorted_trials);

		 $('#sortby button').removeAttr('disabled');

		 button.prop('disabled', true);

	     });

	 }

	 $('#drug-query, #indication-query').bind('keypress', function(e) {
	     if (e.keyCode == 13) {
		 ctviewer_search();
	     }
	 });

	 $(window).scroll( function () {
	     $('#ctvYearMarkerContainer').css('top', $(this).scrollTop() + 50);
	 });

	 $(document).bind('keydown', function(keyStrokeEvent) {
	     if (keyStrokeEvent.keyCode == 74) { // 'j' is pressed
		 if ($('.selectedTrial').length) { // there is a selected trial
		     $('.selectedTrial').last().next().click();
		 } else { // no selected trial
		     $('.ctvTrial').first().click();
		 }

	     }
	     if (keyStrokeEvent.keyCode == 75) { // 'k' is pressed
		 $('.selectedTrial').first().prev().click();
	     }
	     if (keyStrokeEvent.keyCode == 67) { // 'c' is pressed
		 hide_trial_details();
	     }
	 });

	 $(document).ready(function () {

	     // Fix the navigation bar

	     if ( $('#ctvDrugViewGrid').length ) {

		 if ( $("#nav-container").width() < $('#ctvDrugViewGrid').width() ) {
		     $("#nav-container").width(<?php echo $viewerwidth + 350; ?>);
		 }

	     }

	     // Sort FDA info

	     var $fdainfo = $('.fdaDocument, .fdaSubmission, .fdaPMC');

	     var sorted_fda_info = $fdainfo.sort(function (a, b) {
		 return $(a).attr('fdaSortDate') > $(b).attr('fdaSortDate') ? 1 : -1;
	     });

	     $('.fdaDocument, .fdaSubmission').remove();

	     $('#fdaInfoContainer').append(sorted_fda_info);

	     // Sort trials, if necessary

	     if ( '<?php echo $ctviewer['sort_by']; ?>' != 'ctvSortStartDate' ) {
		 $('#' + '<?php echo $ctviewer['sort_by']; ?>' + 'Button').click();
	     }

	 });
	 
	</script>
    </body>
</html>
